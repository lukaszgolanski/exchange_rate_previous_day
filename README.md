# accounting-and-finance-tools
Function to calculate PLN value of money in different currencies. Calculation is based on Polish National Bank (NBP) rates, and can be used for tax calculation. FX is always from the last working day preceding given date.
